import React from 'react';
import image1 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/defectivesarticle.jpg';
import image2 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/globalarticle.jpg';
import image3 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/fairarticle.jpg';
import image4 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/talksarticle.jpg';
import image5 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/dropinarticle.jpg';

const ArticlesList = () => (
  <>
  <main>
  <section id="articlescontent">
  <h1> All of our articles in one place.</h1>
   <h3> This is your one stop shop to keep up to date with the current events.</h3>
   <p> We have anything from career's fair to ROQ Defectives challenge. </p>
   <h4> Think we have missed an event which needs to be covered? Get in touch via our Contact Us Page. </h4>
  </section>
  <section id="articles2">
    <div class="typewriter" id="title">
      <h1>Our Latest Article</h1>
    </div>
    <div class="grid-container">
      <div>  </div>
      <div> <a href="/articles/Defectives"><img src={image1} id="articleicon"/></a>
      <p> ROQ Defectives Challenge </p>
      </div>
      <div>  </div>
    </div>
    </section>
    <section id="articles1">
    <div class="typewriter" id="title">
      <h1>Our Editors Picks</h1>
    </div>
    <div class="grid-container">
      <div>
        <a href="/articles/GlobalGames"><img src={image2} id="articleicon"/></a>
        <p> Global Games </p>
      </div>
      <div>

      </div>
      <div>
        <a href="/articles/GuestTalks"><img src={image4} id="articleicon"/></a>
        <p> Guest Talks</p>
      </div>
    </div>
  </section>

  <section id="articles3">
  <div class="typewriter" id="title">
    <h1> Career's and Advice</h1>
  </div>
  <div class="grid-container">
    <div>
      <a href="/articles/ScienceFair"><img src={image3} id="articleicon"/></a>
      <p> Career's Fair </p>
    </div>
    <div>

    </div>
    <div>
      <a href="/articles/DropIn"><img src={image5} id="articleicon"/></a>
      <p>  Drop In</p>
    </div>
  </div>
</section>
  </main>

  </>
);

export default ArticlesList;
