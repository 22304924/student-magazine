import React from 'react';
import image1 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/contact.jpg';

const ContactUs = () => (
  <>
<main>
  <section id="contactcontent">
    <h1> Have any recommendations you would like us to have a look at?</h1>
    <h4> Complete our contact form on this page with your suggestion including contact number and email address. </h4>
    <h4> We will be in contact if we require any futher assistance or feedback following your suggestion. </h4>
    <h4> Alternatively please email csstudntmagazine@edgehill.ac.uk for assistance.</h4>

  </section>
<div id="contactform">
  <form>
    <p>Full name:</p>
    <input type="text"  />
    <p>Email Address:</p>
    <input type="text"  />
    <p>Contact Number:</p>
    <input type="number"  />
    <p>Leave A Message:</p>
    <textarea  id="message" rows="28" cols="70">
    </textarea>
    <button> Submit </button>
  </form>
  </div>
  <img src={image1} id="contactimage"/>
</main>


  </>
);

export default ContactUs;
