import React from 'react';
import image1 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/defectivesarticle.jpg';
import image2 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/globalarticle.jpg';
import image3 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/fairarticle.jpg';
import image4 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/talksarticle.jpg';

const HomePage = () => (
  <>
  <head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"/>
  </head>
  <main>
  <header>
  <div class="typewriter" id="title">
    <h1>Welcome to Computer Science Student Magazine.</h1>
  </div>
  </header>
<section id="homepagecontent">
<h1> Welcome to our new Computer Science Online Magazine</h1>
 <h3> This is your one stop shop to keep up to date with the current events.</h3>
 <p> We have anything from career's fair to ROQ Defectives challenge. </p>
 <p>To get started you can either click our most recent article located to the right along with our editors picks, or navigate your way through to browse all articles available.</p>
 <h4> Think we have missed an event which needs to be covered? Get in touch via our Contact Us Page. </h4>
</section>
<section id="articles1">
  <div class="typewriter" id="title">
    <h1>Our Latest Article</h1>
  </div>
  <div class="grid-container">
    <div>  </div>
    <div> <a href="/articles/Defectives"><img src={image1} id="articleicon"/></a>
    <p> ROQ Defectives Challenge </p>
    </div>
    <div>  </div>
  </div>
  <div class="typewriter" id="title">
    <h1>Our Editors Picks</h1>
  </div>
  <div class="grid-container">
    <div>
      <a href="/articles/GlobalGames"><img src={image2} id="articleicon"/></a>
      <p> Global Games </p>
    </div>
    <div>

    </div>
    <div>
      <a href="/articles/GuestTalks"><img src={image4} id="articleicon"/></a>
      <p> Guest Talks</p>
    </div>
  </div>
</section>

</main>


  </>
);

export default HomePage;
