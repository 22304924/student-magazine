import React from 'react';
import image1 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/FairSign.jpg';
import image2 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/join.jpg';


const ScienceFair = () => (
  <>
  <main>
  <article>
  <img src={image1} id="fair"/>
  <h1 id="fairtitle"> The Computer Science Career Fair </h1>
  <section id="careerfair">
  <h1> It's back and bigger than ever! </h1>
  <p> The Computer Science Career Fair is back on the 31st January 2020 </p>
  <p> This event will take place in the Tech Hub and will be on from 12 noon till 2pm.</p>
  <p> Companies from the North West will be here for one thing only.</p>
  </section>
  <img src={image2} id="join"/>
  <section id="careerfairlist">
  <p> Some of the companies which are attending this event are: </p>
  <ul>
    <li>ROQ</li>
    <li>Barclays</li>
    <li>Doris IT</li>
    <li> Nuttersons </li>
    <li> Paramount Digital </li>
  </ul>
  </section>
  <div id="careerfairtips">
  <p> To make sure that everyone gets a head start when it comes to this spectatular event we encourage everyone to follow some simple tips.</p>
  <ol>
  <li> Keep an updated C.V to hand. Hand out to as many companies as you wish.</li>
  <li> Ensure you manage your time if you are limited, this will ensure you are able to speak to the companies you wish.</li>
  <li> Research about the companies who are attending. The recruitment guys like to know you have done some reading about their company and what services they provide. </li>
  </ol>
  </div>
  </article>
  </main>
  </>
);

export default ScienceFair;
