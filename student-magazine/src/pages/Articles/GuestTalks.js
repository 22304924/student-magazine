import React from 'react';
import image1 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/talk.jpg';
import image2 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/crowd.jpg';

const GuestTalks = () => (
  <>
  <main>
  <article>
  <img src={image1} id="lecturetalk"/>
  <h1 id="talktitle"> Guest Talks </h1>
  <div id="guestcontent">
    <h3> This year at Edge Hill we have multiple guest speakers to talk about topics of thier choice. </h3>
    <h4> Some of the companies in which are attending including ROQ, National Crime Agency and AO.com </h4>
    <p> These companies have been asked to supply us with a lecture on a topic of their choice. This will allow us into a insight into their company including what they are currently developing / working on. </p>
    <p> Any details for this event can be found upon BlackBoard. </p>
  </div>
  <table id="guesttable">
  <tr>
    <th>Time</th>
    <th>Company</th>
    <th>Topic</th>
    <th>Location</th>
  </tr>
  <tr>
    <td>
      2:00pm – 2:30pm
    </td>
    <td>
      National Crime Agency
    </td>
    <td>
      Cyber Crime / Law Enforcement
    </td>
    <td>
      Tech Hub Lecture Theatre
    </td>
  </tr>
  <tr>
    <td>
      2:30pm – 3:00pm
    </td>
    <td>
      CTI Digital / NuBlue
    </td>
    <td>
      Future Commerce – The Future of online retail
    </td>
    <td>
      Tech Hub Lecture Theatre
    </td>
  </tr>

  <tr>
    <td>
      3:00pm – 3:30pm
    </td>
    <td>
      AO.com
    </td>
    <td>
      How to shine in a technical interview
    </td>
    <td>
      Tech Hub Lecture Theatre
    </td>
  </tr>

  <tr>
    <td>
      3:30pm – 4:00pm
    </td>
    <td>
      DXC
    </td>
    <td>
      Digital Transformation
    </td>
    <td>
      Tech Hub Lecture Theatre
    </td>
  </tr>

  <tr>
    <td>
      4:00pm – 4:30pm
    </td>
    <td>
      ROQ
    </td>
    <td>
      Careers in Testing
    </td>
    <td>
      Tech Hub Lecture Theatre
    </td>
  </tr>

  </table>
  <section id="guestfooter">
  <h3> If you would like to book onto this event please visit <u>https://ehucomputersceicneguesttalks.eventbrite.co.uk</u></h3>
  <h3> Contact <u>csevents@edgehill.ac.uk</u> for more information. </h3>
  </section>
  <img src={image2} id="crowd"/>
  </article>
  </main>
  </>
);

export default GuestTalks;
