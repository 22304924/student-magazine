import React from 'react';

import ScrollAnimation from 'react-animate-on-scroll';

const GlobalGames = () => (
  <>
  <head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"/>
  </head>
  <main>
  <article id="stars">
    <div id='globaltitle'> Global Games Jam 2020 </div>
    <p id="context"> 24 Students were tasked with an unusual but fun task! They have 48 hours to create a game based on an set of objectives and requirements.</p>
    <div class="planet">
      <div class="wrap">
        <div class="background"></div>
        <div class="clouds"></div>
      </div>
      <div class="mask"></div>
    </div>
    <div>
    <ScrollAnimation duration={3}
    animateIn='zoomIn'>
    <div id="globalgamesarticle1">
      <p> On the 24th of March, 24 of our Computer Science students was set 1 task and only 1.</p>
      <p> They had 48 hours to design and develop a game hich had to meet specific requirements / guidelines which are provided</p>
    </div>
    </ScrollAnimation>
    <ScrollAnimation duration={3}
    animateIn='zoomIn'>
    <div id="globalgamesarticle2">
      <p> Some of the games which was created was a zombie survival based on Dead Rising, a strategy game based on Dawn Of War and a puzzle game based on Portal.</p>
      <p> All of the games which were created for the challenge was assessed by our independant judges. </p>
      <p> After plenty of consideration our judges finally decided who they found met the criteria in which was asked. </p>
    </div>
    </ScrollAnimation>
    <ScrollAnimation duration={3}
    animateIn='zoomIn'>
    <div id="globalgamesarticle3">
      <p> Without further suspense ... </p>
      <p> The Winner of the Gobal Games Jam 2020 is ..... </p>
      <p> .......... </p>
      <p> This team won with the game .... based upon the puzzle game Portal. </p>
    </div>
    </ScrollAnimation>
    <ScrollAnimation duration={3}
    animateIn='zoomIn'>
    <div id="globalgamesarticle">
      <p> If you would like to register your interest in this event please email 'csevents@edgehill.ac.uk'</p>
      <p> Full demonstration and images of the event can be found on BlackBoard under the organisation tab. </p>
    </div>
    </ScrollAnimation>
    </div>
  </article>
  </main>

  </>
);

export default GlobalGames;
