import React from 'react';
import ScrollAnimation from 'react-animate-on-scroll';
import image2 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/laptop.jpg';

const DropIn = () => (
    <>
    <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"/>
    </head>
    <main id="DropIn">
    <article>
    <img src={image2} id="laptop" />

    <h1 id="dropintitle"> Computer Sciece Drop In </h1>

    <ScrollAnimation duration={3}
    animateIn='zoomIn'>

    <section id="dropincontent">
    <p> The drop in session will be for any Computer Science students to come along and speak with the Employability team. This could be about: </p>
    <ul>
      <li>Computer Science Careers Fair</li>
      <li>Global Games Jam</li>
      <li>Placements</li>
      <li>Extra Curricular Certificates</li>
      <li>Applying for a graduate job / placement</li>
      <li>Part time work</li>
    </ul>
    <p> No booking required just come along </p>
    </section>
    </ScrollAnimation>

    </article>
    </main>
    </>
  );

  export default DropIn;
