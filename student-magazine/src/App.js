import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import HomePage from './pages/HomePage';
import Articles from './pages/ArticlesList';
import GlobalGames from './pages/Articles/GlobalGames';
import Defectives from './pages/Articles/Defectives';
import GuestTalks from './pages/Articles/GuestTalks';
import ScienceFair from './pages/Articles/ScienceFair';
import DropIn from './pages/Articles/DropIn';
import NavBar from './NavBar';
import ContactUs from './pages/ContactUs';


import './App.css';

function App() {

  return (
    <Router>
    <NavBar />
        <Route path="/" component={HomePage} exact />
        <Route path="/ArticlesList" component={Articles} />
        <Route path='/Articles/GlobalGames' component={GlobalGames} />
        <Route path='/Articles/Defectives' component={Defectives} />
        <Route path='/Articles/GuestTalks' component={GuestTalks} />
        <Route path='/Articles/ScienceFair' component={ScienceFair} />
        <Route path='/Articles/DropIn' component={DropIn} />
        <Route path='/ContactUs' component={ContactUs} />
    </Router>

  );
}

export default App;
