import React from 'react';
import { Link } from 'react-router-dom';
import image1 from '/Users/dale/Desktop/University/Year3/Diss/student_magazine/student-magazine/student-magazine/src/pages/Components/logo.jpg';


const NavBar = () => (
  <>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>

    <div id="mySidenav" class="sidenav">
    <img src={image1} id="logo"/> 
      <Link to="/"><i class="fa fa-fw fa-home"></i> Home</Link>
      <Link to='/ArticlesList'><i class="fa fa-fw fa-book"></i>Articles</Link>
      <Link to='/ContactUs'><i class="fa fa-fw fa-phone"></i>Contact Us</Link>
    </div>

    </>
);

export default NavBar;
